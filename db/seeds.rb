# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#Roles
admin_role = Role.new :name => :admin
normal_role = Role.new :name => :normal
admin_role.save
normal_role.save

#Uers
admin = User.new  :password => rand(36**12).to_s(36),
                  :email    => 'admin@appinfo.com'

admin.roles << admin_role

admin.skip_confirmation!
admin.save


normal_role = Role.find_by_name(:normal)
User.all.each do |u|
  if !u.role? :normal
    u.roles << normal_role
    u.save
  end
end


puts
puts
puts '---------------------------------------'
puts '|          Admin Credentials          |'
puts '---------------------------------------'
puts "Admin Email: #{admin.unconfirmed_email}"
puts "Admin Password: #{admin.password}"
puts '---------------------------------------'
puts
puts