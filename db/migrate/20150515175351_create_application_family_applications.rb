class CreateApplicationFamilyApplications < ActiveRecord::Migration
  def change
    create_table :application_family_applications do |t|
      t.integer :application_id
      t.integer :application_family_id
      t.boolean :ignore_active, :default => false
      t.datetime :last_checked_at, :null => false, :default => Time.now
      t.boolean :active, :default => true

      t.timestamps
    end
  end
end
