class CreateApplicationVersions < ActiveRecord::Migration
  def change
    create_table :application_versions do |t|
      t.string :application_version
      t.belongs_to :server
      t.belongs_to :application

      t.timestamps
    end
  end
end
