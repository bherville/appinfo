class AddMajorApplicationColumnToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :major_application, :boolean, :default => false
  end
end
