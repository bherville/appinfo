class RemoveVersionFromApplications < ActiveRecord::Migration
  def up
    remove_column :applications, :version
  end

  def down
    add_column :applications, :version, :string
  end
end
