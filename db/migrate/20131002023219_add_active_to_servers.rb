class AddActiveToServers < ActiveRecord::Migration
  def change
    add_column :servers, :active, :boolean
  end
end
