class AddIgnoreActiveColumnToServers < ActiveRecord::Migration
  def change
    add_column :servers, :ignore_active, :boolean, :default => false
  end
end
