class AddSendsEmailColumnToApplications < ActiveRecord::Migration
  def change
    add_column :applications, :sends_emails, :boolean, :default => false
  end
end
