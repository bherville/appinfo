class CreateServerApplications < ActiveRecord::Migration
  def change
    create_table :server_applications do |t|
      t.integer :server_id
      t.integer :application_id
      t.integer :user_id
      t.boolean :ignore_active, :default => false
      t.datetime :last_checked_at, :null => false, :default => Time.now
      t.boolean :active, :default => true
      t.text :description

      t.timestamps
    end
  end
end
