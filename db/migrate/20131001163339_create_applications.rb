class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string :name
      t.string :app_type
      t.string :applications, :version

      t.timestamps
    end
  end
end
