class AddNormalRoleToAllUsers < ActiveRecord::Migration
  def change
    admin_role = Role.new :name => :admin
    normal_role = Role.new :name => :normal
    admin_role.save
    normal_role.save

    normal_role = Role.find_by_name(:normal)
    User.all.each do |u|
      if !u.role? :normal
        u.roles << normal_role
        u.save
      end
    end
  end
end
