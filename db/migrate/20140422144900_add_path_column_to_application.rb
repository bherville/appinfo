class AddPathColumnToApplication < ActiveRecord::Migration
  def change
    add_column :applications, :path, :string
  end
end
