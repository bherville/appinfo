class RemoveTableApplicationsServers < ActiveRecord::Migration
  def up
    drop_table :applications_servers
  end

  def down
    create_table :applications_servers, :id => false do |t|
      t.references :server
      t.references :application
    end
  end
end
