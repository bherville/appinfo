class AddUserIdToApplicationVersion < ActiveRecord::Migration
  def change
    add_column :application_versions, :user_id, :integer
  end
end
