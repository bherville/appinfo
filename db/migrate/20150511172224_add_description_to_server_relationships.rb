class AddDescriptionToServerRelationships < ActiveRecord::Migration
  def change
    add_column :server_relationships, :description, :text
  end
end
