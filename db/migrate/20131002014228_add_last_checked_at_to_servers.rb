class AddLastCheckedAtToServers < ActiveRecord::Migration
  def change
    add_column :servers, :last_checked_at, :datetime
  end
end
