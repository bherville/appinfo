class CreateApplicationsServers < ActiveRecord::Migration
  def up
    create_table :applications_servers, :id => false do |t|
      t.references :server
      t.references :application
    end
  end

  def down
    drop_table :applications_servers
  end
end
