class AddVersionCommandColumnToApplication < ActiveRecord::Migration
  def change
    add_column :applications, :version_command, :text
  end
end
