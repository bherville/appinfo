class AddActiveColumns < ActiveRecord::Migration
  TABLES = [:applications, :application_versions, :databases, :server_databases, :server_relationships]

  def self.up
    TABLES.each do |t|
      add_column t, :ignore_active, :boolean, :default => false
      add_column t, :last_checked_at, :datetime, :null => false, :default => Time.now
      add_column t, :active, :boolean, :default => true
    end
  end

  def self.down
    TABLES.each do |t|
      remove_column t, :ignore_active
      remove_column t, :last_checked_at
      remove_column t, :active
    end
  end
end
