class AddUniqueIndexes < ActiveRecord::Migration
  def up
    add_index :application_family_applications, [ :application_family_id, :application_id ], :unique => true, :name => :by_application_family_and_application
    add_index :application_versions, :server_application_id, :unique => true, :name => :by_server_application_id
    add_index :server_applications, [ :application_id, :server_id ], :unique => true, :name => :by_application_and_server
    add_index :server_databases, [ :database_id, :server_id ], :unique => true, :name => :by_database_and_server
    add_index :server_relationships, [ :server_primary_id, :server_secondary_id, :port ], :unique => true, :name => :by_server_primary_and_server_secondary_and_port
  end

  def down
    remove_index :application_family_applications, name: :by_application_family_and_application
    remove_index :server_relationships, name: :by_server_application_id
    remove_index :server_applications, name: :by_application_and_server
    remove_index :server_databases, name: :by_database_and_server
    remove_index :server_relationships, name: :by_server_primary_and_server_secondary_and_port
  end
end
