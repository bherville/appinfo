# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150824191402) do

  create_table "application_families", :force => true do |t|
    t.string   "name"
    t.boolean  "ignore_active",   :default => false
    t.datetime "last_checked_at", :default => '2015-05-15 20:01:31', :null => false
    t.boolean  "active",          :default => true
    t.integer  "user_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
  end

  create_table "application_family_applications", :force => true do |t|
    t.integer  "application_id"
    t.integer  "application_family_id"
    t.boolean  "ignore_active",         :default => false
    t.datetime "last_checked_at",       :default => '2015-05-15 20:01:32', :null => false
    t.boolean  "active",                :default => true
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
  end

  add_index "application_family_applications", ["application_family_id", "application_id"], :name => "by_application_family_and_application", :unique => true

  create_table "application_versions", :force => true do |t|
    t.text     "application_version"
    t.integer  "server_id"
    t.integer  "application_id"
    t.datetime "created_at",                                               :null => false
    t.datetime "updated_at",                                               :null => false
    t.boolean  "ignore_active",         :default => false
    t.datetime "last_checked_at",       :default => '2015-05-10 14:27:51', :null => false
    t.boolean  "active",                :default => true
    t.integer  "user_id"
    t.integer  "server_application_id"
  end

  add_index "application_versions", ["server_application_id"], :name => "by_server_application_id", :unique => true

  create_table "applications", :force => true do |t|
    t.string   "name"
    t.string   "app_type"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.integer  "user_id"
    t.boolean  "sends_emails",      :default => false
    t.string   "path"
    t.text     "version_command"
    t.boolean  "major_application", :default => false
    t.boolean  "ignore_active",     :default => false
    t.datetime "last_checked_at",   :default => '2015-05-10 14:27:51', :null => false
    t.boolean  "active",            :default => true
  end

  create_table "databases", :force => true do |t|
    t.string   "name"
    t.string   "dbms"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.boolean  "ignore_active",   :default => false
    t.datetime "last_checked_at", :default => '2015-05-10 14:27:51', :null => false
    t.boolean  "active",          :default => true
    t.integer  "application_id"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "server_applications", :force => true do |t|
    t.integer  "server_id"
    t.integer  "application_id"
    t.integer  "user_id"
    t.boolean  "ignore_active",   :default => false
    t.datetime "last_checked_at", :default => '2015-06-02 16:04:48', :null => false
    t.boolean  "active",          :default => true
    t.text     "description"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
  end

  add_index "server_applications", ["application_id", "server_id"], :name => "by_application_and_server", :unique => true

  create_table "server_databases", :force => true do |t|
    t.integer  "server_id"
    t.integer  "database_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.boolean  "ignore_active",   :default => false
    t.datetime "last_checked_at", :default => '2015-05-10 14:27:51', :null => false
    t.boolean  "active",          :default => true
  end

  add_index "server_databases", ["database_id", "server_id"], :name => "by_database_and_server", :unique => true

  create_table "server_relationships", :force => true do |t|
    t.integer  "server_primary_id"
    t.integer  "server_secondary_id"
    t.integer  "port"
    t.datetime "created_at",                                             :null => false
    t.datetime "updated_at",                                             :null => false
    t.boolean  "ignore_active",       :default => false
    t.datetime "last_checked_at",     :default => '2015-05-10 14:27:51', :null => false
    t.boolean  "active",              :default => true
    t.text     "description"
  end

  add_index "server_relationships", ["server_primary_id", "server_secondary_id", "port"], :name => "by_server_primary_and_server_secondary_and_port", :unique => true

  create_table "servers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "user_id"
    t.datetime "last_checked_at"
    t.boolean  "active"
    t.boolean  "ignore_active",   :default => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",                           :null => false
    t.string   "encrypted_password",     :default => "",                           :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at",                                                       :null => false
    t.datetime "updated_at",                                                       :null => false
    t.string   "time_zone",              :default => "Eastern Time (US & Canada)"
    t.string   "provider"
    t.integer  "uid"
    t.string   "fname"
    t.string   "lname"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

end
