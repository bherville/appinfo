class Swagger::Docs::Config
    def self.transform_path(path, version)
        "#{ENV['APPINFO_HOSTNAME']}/swagger_docs/#{path}"
    end
end
Swagger::Docs::Config.register_apis({
    "1.0" => {
        # the extension used for the API
        :api_extension_type => :json,
        # the output location where your .json files are written to
        :api_file_path => "public/swagger_docs",
        # the URL base path to your API
        :base_path => "#{ENV['APPINFO_API_HOST']}",
        # if you want to delete all .json files at each generation
        :base_api_controller => "API::APIBaseController",
        :clean_directory => true,
    }
})