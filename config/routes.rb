Appinfo::Application.routes.draw do
  root :to => 'servers#index'

  devise_for :users, :controllers => { :registrations => 'registrations', :omniauth_callbacks => 'users/omniauth_callbacks'}

  namespace :admin do
    match '/' => 'admin#index'
    resources :users
  end

  resources :applications
  resources :servers, :id => /[A-Za-z0-9\.]+?/, :format => /html|csv/
  resources :token_authentications, :only => [:create, :destroy]
  resources :application_versions
  resources :server_relationships
  resources :server_databases
  resources :databases
  resources :application_families
  resources :application_family_applications
  resources :server_applications
  resource :user, only: [:show]

  namespace :api, :defaults => {:format => :html} do
    root :to => 'api#index'

    resources :api, :controller => :api

    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: false), :defaults => {:format => :json} do
      resources :application_versions do
        collection do
          get :exists
        end
      end
      resources :applications do
        resources :application_versions
        collection do
          get :exists
        end
      end
      resources :servers, :id => /[\w.]+?/  do
        resources :application_versions
        resources :applications
        put :associate_application
        get :has_application
      end
      resources :server_relationships do
        collection do
          get :exists
        end
      end
      resources :server_databases do
        collection do
          get :exists
        end
      end
      resources :databases do
        collection do
          get :exists
        end
      end
      resources :application_families
      resources :application_family_applications do
        collection do
          get :exists
        end
      end
      resources :server_applications do
        collection do
          get :exists
        end
      end
    end

    scope module: :v2, constraints: ApiConstraints.new(version: 2, default: true), :defaults => {:format => :json} do
      resources :applications, :id => /[\w\d%\-]+?/ do
        resources :servers, :id => /[A-Za-z0-9\.]+?/, :only => [:index, :show], :format => /json|csv|xml|yaml/ do
          resource :application_version
        end
        resources :databases, :id => /[\w\d%\-]+?/
        resources :server_applications, :only => [:index, :show]
      end
      resources :servers, :id => /[A-Za-z0-9\.]+?/, :format => /json/ do
        resources :applications, :id => /[\w\d%\-]+?/  do
          resource :application_version
          resources :server_applications
        end
        resources :server_applications do
          resource :application_version
        end
        resources :databases, :only => [:index, :show], :id => /[\w\d%\-]+?/ do
          resources :server_databases
        end
        resources :servers, :id => /[A-Za-z0-9\.]+?/, :format => /json/
        resources :server_relationships
        resources :server_databases
      end
      resources :databases, :only => [:index, :show], :id => /[\w\d%\-]+?/ do
        resource :application, :id => /[\w\d%\-]+?/
        resources :server_databases, :only => [:index, :show]
      end
      resources :application_families do
        resources :application_family_applications
      end
      resources :application_versions, :only => [:index, :show]
      resources :server_applications, :only => [:index, :show]
      resources :server_databases, :only => [:index, :show]
      resources :server_relationships, :only => [:index, :show]
      resources :application_family_applications, :only => [:index, :show]
    end
  end
end
