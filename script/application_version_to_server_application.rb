app_versions = ApplicationVersion.all
app_versions.each do |av|
	next unless av.server_id && av.application_id
	server_application = ServerApplication.find_by_application_id_and_server_id(av.application_id, av.server_id)
	server_application = ServerApplication.create(:server_id => av.server_id, :application_id => av.application_id) unless server_application
	av.server_application = server_application
	av.save!
end

app_versions = ApplicationVersion.all
app_versions.each do |av|
	next if av.server_application
	av.destroy
end