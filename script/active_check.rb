servers = Server.all

servers.each do |s|
  if s.is_active?
    puts "#{s.name} - active"
    s.set_active true
  else
    puts "#{s.name} - inactive"
    s.set_active false
  end
end