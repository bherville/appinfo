namespace :servers do
  desc 'Check to see if any servers are no longer active and if they are not the set them to inactive'
  task :active_check => :environment do
    inactive_only = ENV['inactive_only'] ? (ENV['inactive_only'] == 'true' ? true : false) : false

    servers = Server.all

    servers.each do |s|
      if s.is_active?
        puts "#{s.name} - active" unless inactive_only
        s.set_active true
      else
        puts "#{s.name} - inactive"
        s.set_active false
      end
    end
  end

end
