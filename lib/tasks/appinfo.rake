namespace :appinfo do
  def get_stale_record_seconds
    ENV['STALE_RECORD_SECONDS'] ? ENV['STALE_RECORD_SECONDS'].to_i : 1.month.ago.to_i
  end

  def process_model(model, stale_record_seconds)
    resources = model.all

    resources.each do |r|
      if r.is_active? stale_record_seconds.seconds.ago
        r.set_active true
      else
        r.set_active false
      end
    end
  end

  desc "Check all resource types to see if they are active"
  task :active_check_all => :environment do
    stale_record_seconds = get_stale_record_seconds

    models = [Application, ApplicationVersion, Database, Server, ServerDatabase, ServerRelationship]

    models.each do |m|
      puts "Processing #{m.to_s}"
      process_model m, stale_record_seconds
    end
  end

  desc "Reset the active state of all resources"
  task :active_reset_all => :environment do
    reset_to = ENV['RESET_TO'] ? (ENV['RESET_TO'] == 'true' ? true : false) : false

    puts "Reseting all resource active state to #{reset_to}!"

    models = [Application, ApplicationVersion, Database, Server, ServerDatabase, ServerRelationship]

    models.each do |m|
      puts "Processing #{m.to_s}"
      m.all.each do |r|
        r.set_active(reset_to)
      end
    end
  end
end
