require 'test_helper'

class ApplicationFamilyApplicationsControllerTest < ActionController::TestCase
  setup do
    @application_family_application = application_family_applications(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:application_family_applications)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create application_family_application" do
    assert_difference('ApplicationFamilyApplication.count') do
      post :create, application_family_application: { application_family_id: @application_family_application.application_family_id, application_id: @application_family_application.application_id }
    end

    assert_redirected_to application_family_application_path(assigns(:application_family_application))
  end

  test "should show application_family_application" do
    get :show, id: @application_family_application
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @application_family_application
    assert_response :success
  end

  test "should update application_family_application" do
    put :update, id: @application_family_application, application_family_application: { application_family_id: @application_family_application.application_family_id, application_id: @application_family_application.application_id }
    assert_redirected_to application_family_application_path(assigns(:application_family_application))
  end

  test "should destroy application_family_application" do
    assert_difference('ApplicationFamilyApplication.count', -1) do
      delete :destroy, id: @application_family_application
    end

    assert_redirected_to application_family_applications_path
  end
end
