class Application < ActiveRecord::Base
  attr_accessible :name, :app_type, :sends_emails, :path, :version_command, :major_application, :ignore_active, :active, :application_families

  validates :name, presence: true,
                   uniqueness: true

  belongs_to :user

  has_many :application_family_applications, :dependent => :destroy
  has_many :server_applications, :dependent => :destroy

  has_many :servers, through: :server_applications
  has_many :application_families, through: :application_family_applications, :uniq => true

  has_many :databases

  before_save :fix_case
  before_save :replace_string
  before_create :initialize_last_checked
  before_update :initialize_last_checked

  extend FriendlyId
  friendly_id :name

  def to_s
    self.name
  end

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end

  def fix_case
    if self.app_type
      self.app_type = self.app_type.downcase
    end
  end

  def replace_string
    if self.version_command
      self.attributes.each_pair do |name, value|
        self.version_command = self.version_command.gsub("%#{name}%", value.to_s)
      end
    end
  end
end
