class ServerApplication < ActiveRecord::Base
  attr_accessible :description, :ignore_active, :active, :application_id, :server_id, :application, :server

  validates :application_id, :presence => true
  validates :server_id, :presence => true
  validates_uniqueness_of :server_id, :scope => :application_id

  belongs_to :application
  belongs_to :server
  belongs_to :user

  has_one :application_version, :dependent => :destroy

  before_create :initialize_last_checked
  before_create :build_attributes
  before_update :initialize_last_checked

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!

    self.application_version.update_last_checked
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end

  def build_attributes
    self.build_application_version
  end
end
