class ApplicationFamily < ActiveRecord::Base
  attr_accessible :last_checked_at, :name, :ignore_active, :active, :applications, :application_ids, :ignore_active, :active

  validates :name, :presence => true,
                   :uniqueness => true

  has_many :application_family_applications, :dependent => :destroy

  has_many :applications, through: :application_family_applications, :uniq => true

  belongs_to :user

  before_create :initialize_last_checked
  before_update :initialize_last_checked

  extend FriendlyId
  friendly_id :name

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end
end
