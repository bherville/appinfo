class Database < ActiveRecord::Base
  attr_accessible :name, :dbms, :servers, :ignore_active, :active, :application_id, :application

  validates :name,            :presence => true,
                              :uniqueness => true
  validates :application_id,  :presence => true


  before_save :fix_case
  before_create :initialize_last_checked
  before_update :initialize_last_checked

  has_many :server_databases, :dependent => :destroy
  has_many :servers, through: :server_databases
  belongs_to :application

  extend FriendlyId
  friendly_id :name

  def full_name
    "#{self.dbms} - #{self.name}"
  end

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end

  def fix_case
    self.dbms = self.dbms.downcase if self.dbms
  end
end
