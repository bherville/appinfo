class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user

    if user.role? :admin
      can :manage, :all
    elsif user.role? :normal
      can :manage, [Server, Application, ApplicationFamily, ApplicationFamilyApplication, ApplicationVersion, ServerRelationship, ServerDatabase]
      can :manage, User do |user|
        user.try(:owner) == user
      end
    else
      can :read, [Server, Application]
    end
  end
end
