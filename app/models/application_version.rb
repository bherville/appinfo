class ApplicationVersion < ActiveRecord::Base
  attr_accessible :application, :server, :application_version, :ignore_active, :active, :server_application, :server_application_attributes

  belongs_to :user
  belongs_to :server_application

  has_one :application, :through => :server_application
  has_one :server, :through => :server_application

  accepts_nested_attributes_for :server_application

  before_create :initialize_last_checked
  before_update :initialize_last_checked

  validates :server_application_id, :presence => true, :uniqueness => true


  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end
end
