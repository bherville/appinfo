class ServerRelationship < ActiveRecord::Base
  attr_accessible :port, :server_primary_id, :server_secondary_id, :server_primary, :server_secondary, :ignore_active, :active, :description

  belongs_to :server_primary, :foreign_key => :server_primary_id, :class_name => 'Server'
  belongs_to :server_secondary, :foreign_key => :server_secondary_id, :class_name => 'Server'

  validates :server_primary_id, :presence => true
  validates :server_secondary_id, :presence => true

  validates_uniqueness_of :port, :scope => [:server_secondary_id, :server_primary_id]

  before_create :initialize_last_checked
  before_update :initialize_last_checked

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end
end
