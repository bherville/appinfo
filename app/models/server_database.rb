class ServerDatabase < ActiveRecord::Base
  attr_accessible :database_id, :server_id, :ignore_active, :active

  validates_uniqueness_of :database_id, :scope => :server_id

  belongs_to :database
  belongs_to :server

  before_create :initialize_last_checked
  before_update :initialize_last_checked

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end
end
