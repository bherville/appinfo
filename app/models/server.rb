class Server < ActiveRecord::Base
  attr_accessible :name, :applications, :active, :application_ids, :ignore_active, :databases

  before_create :downcase_fields, :initialize_last_checked
  before_destroy :remove_associations
  before_update :initialize_last_checked

  validates :name, presence: true, :uniqueness => true

  belongs_to :user

  has_many :server_databases, :dependent => :destroy
  has_many :server_applications, :dependent => :destroy
  has_many :server_relationships, :foreign_key => 'server_primary_id', :class_name => 'ServerRelationship', :dependent => :destroy

  has_many :applications, through: :server_applications
  has_many :databases, through: :server_databases
  has_many :servers, through: :server_relationships, :source => 'server_secondary'

  extend FriendlyId
  friendly_id :name

  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  def to_s
    self.name
  end

  def normalize_friendly_id(string)
    super.downcase.gsub('-', '.')
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end

  def downcase_fields
    self.name.downcase!
  end

  def remove_associations
    ServerRelationship.find_all_by_server_primary_id(self.id).each do |sr|
      sr.destroy
    end

    ServerRelationship.find_all_by_server_secondary_id(self.id).each do |sr|
      sr.destroy
    end
  end
end
