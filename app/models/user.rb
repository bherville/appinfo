class User < ActiveRecord::Base
  include Gravtastic
  gravtastic :secure => false

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable, :confirmable, :lockable,
         :timeoutable

  devise :omniauthable, :omniauth_providers => [:auth_man]

  has_many :servers, :order => 'name ASC'
  has_many :applications, :order => 'name ASC'
  has_many :application_families, :order => 'name ASC'
  has_many :application_versions
  has_many :server_applications

  has_and_belongs_to_many :roles

  after_create :create_token, :assign_roles

  attr_accessible :email, :password, :password_confirmation, :remember_me, :authentication_token, :role_ids, :time_zone, :fname, :lname

  def role?(role)
    !!self.roles.find_by_name(role.to_s.downcase)
  end

  def self.from_omniauth(auth)
    user_return = where(provider: auth.provider, uid: auth.uid).first

    unless user_return
      user = User.new(
          :email        => auth.info.email,
          :password     => Devise.friendly_token[0,20],
          :fname        => auth.info.fname,
          :lname        => auth.info.lname,
          :time_zone    => auth.info.time_zone,
      )

      user.uid      = auth.uid
      user.provider = auth.provider

      user.skip_confirmation!
      user.save

      user_return = user
    end

    user_return.sync_with_omniauth(auth)

    user_return
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.auth_man_data'] && session['devise.auth_man_data']['extra']['raw_info']
        user.email      = data['email'] if user.email.blank?
        user.fname      = data['fname'] if user.fname.blank?
        user.lname      = data['lname'] if user.lname.blank?
        user.time_zone  = data['time_zone'] if user.time_zone.blank?
      end
    end
  end

  def sync_with_omniauth(auth)
    unless auth.info.fname == self.fname && auth.info.lname == self.lname && auth.info.time_zone == self.time_zone
      self.fname      = auth.info.fname
      self.lname      = auth.info.lname
      self.time_zone  = auth.info.time_zone

      self.save
    end
  end

  def full_name
    "#{self.fname} #{self.lname}"
  end

  def full_name_comma
    "#{self.lname}, #{self.fname}"
  end

  private
  def create_token
    self.reset_authentication_token!
  end

  def assign_roles
    self.roles << Role.find_by_name(:normal)
  end
end
