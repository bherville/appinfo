class ApplicationFamilyApplication < ActiveRecord::Base
  attr_accessible :application_family_id, :application_id, :application_family, :application, :ignore_active, :active

  validates :application_family_id, :presence => true
  validates :application_id, :presence => true

  belongs_to :application
  belongs_to :application_family

  before_create :initialize_last_checked
  before_update :initialize_last_checked


  def update_last_checked
    self.last_checked_at = Time.new
    self.active = true
    self.save!
  end

  def is_active?(length = 1.month.ago)
    if self.ignore_active?
      true
    elsif self.last_checked_at < length
      false
    else
      true
    end
  end

  def set_active(active)
    self.active = active
    self.save
  end

  private
  def initialize_last_checked
    self.last_checked_at = Time.new
  end
end
