json.partial! 'server', server: server
json.server_applications do
  json.array! server.server_applications, partial: "api/#{self.controller_path.split('/').second}/server_applications/server_application", as: :server_application
end