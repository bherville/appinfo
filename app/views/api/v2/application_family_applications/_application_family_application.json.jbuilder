json.id application_family_application.id
json.application do
  json.partial! "api/#{self.controller_path.split('/').second}/applications/application", application: application_family_application.application
end
json.application_family do
  json.partial! "api/#{self.controller_path.split('/').second}/application_families/application_family", application_family: application_family_application.application_family
end
json.active application_family_application.active
json.ignore_active application_family_application.ignore_active
json.last_checked_at application_family_application.last_checked_at