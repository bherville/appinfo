json.partial! 'application_version', application_version: application_version
json.application do
  json.partial! "api/#{self.controller_path.split('/').second}/applications/application", application: application_version.server_application.application
end
json.server do
  json.partial! "api/#{self.controller_path.split('/').second}/servers/server", server: application_version.server_application.server
end