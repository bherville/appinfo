json.active server_application.active
json.ignore_active server_application.ignore_active
json.created_at server_application.created_at
json.id server_application.id
json.last_checked_at server_application.last_checked_at
json.updated_at server_application.updated_at
json.user_id server_application.user_id
json.url server_application_url(server_application)
json.path server_application_path(server_application)
json.description server_application.description
if server_application.application_version
  json.application_version do
    json.partial! "api/#{self.controller_path.split('/').second}/application_versions/application_version", application_version: server_application.application_version
  end
end
if server_application.application
  json.application do
    json.partial! "api/#{self.controller_path.split('/').second}/applications/application", application: server_application.application
  end
end