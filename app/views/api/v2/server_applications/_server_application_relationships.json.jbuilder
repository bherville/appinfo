json.partial! 'server_application', server_application: server_application
if server_application.application_version
  json.application_version do
    json.partial! "api/#{self.controller_path.split('/').second}/application_versions/application_version", application_version: server_application.application_version
  end
end
if server_application.application
  json.application do
    json.partial! "api/#{self.controller_path.split('/').second}/applications/application", application: server_application.application
  end
end
if server_application.server
  json.server do
    json.partial! "api/#{self.controller_path.split('/').second}/servers/server", server: server_application.server
  end
end