json.created_at database.created_at
json.id database.id
json.name database.name
json.dbms database.dbms
json.updated_at database.updated_at
json.active database.active
json.ignore_active database.ignore_active