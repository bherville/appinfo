json.partial! 'database', database: database
json.servers do
  json.array! database.servers, partial: "api/#{self.controller_path.split('/').second}/servers/server", as: :server
end
json.application do
  json.partial! "api/#{self.controller_path.split('/').second}/applications/application", application: database.application
end