json.active server.active
json.ignore_active server.ignore_active
json.created_at server.created_at
json.id server.id
json.last_checked_at server.last_checked_at
json.name server.name
json.updated_at server.updated_at
json.user_id server.user_id
json.url server_url(server)
json.path server_path(server)
json.applications server.application_versions.each do |application|
  json.name application.application.name
  json.version application.application_version
  json.url application_version_url(application)
  json.path application_version_path(application)
  json.major_application application.application.major_application
end