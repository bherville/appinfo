json.database do
  json.partial! "api/#{self.controller_path.split('/').second}/databases/database", database: server_database.database
end
json.server do
  json.partial! "api/#{self.controller_path.split('/').second}/servers/server", server: server_database.server
end
json.active server_database.active
json.ignore_active server_database.ignore_active