json.id server_relationship.id
json.server_primary do
  json.partial! "api/#{self.controller_path.split('/').second}/servers/server", server: server_relationship.server_primary
end
json.server_secondary do
  json.partial! "api/#{self.controller_path.split('/').second}/servers/server", server: server_relationship.server_secondary
end
json.port server_relationship.port
json.description server_relationship.description
json.active server_relationship.active
json.ignore_active server_relationship.ignore_active