json.app_type application.app_type
json.created_at application.created_at
json.id application.id
json.major_application application.major_application
json.name application.name
json.path application.path
json.sends_emails application.sends_emails
json.version_command application.version_command
json.updated_at application.updated_at
json.user_id application.user_id
json.active application.active
json.ignore_active application.ignore_active
json.last_checked_at application.last_checked_at