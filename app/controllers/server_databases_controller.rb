class ServerDatabasesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /server_databases
  def index
    @server_databases = ServerDatabase.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /server_databases/1
  def show
    @server_database = ServerDatabase.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /server_databases/new
  def new
    @server_database = ServerDatabase.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /server_databases/1/edit
  def edit
    @server_database = ServerDatabase.find(params[:id])
  end

  # POST /server_databases
  def create
    @server_database = ServerDatabase.new(params[:server_database])

    respond_to do |format|
      if @server_database.save
        format.html { redirect_to @server_database, notice: 'Server database was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /server_databases/1
  def update
    @server_database = ServerDatabase.find(params[:id])

    respond_to do |format|
      if @server_database.update_attributes(params[:server_database])
        format.html { redirect_to @server_database, notice: 'Server database was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /server_databases/1
  def destroy
    @server_database = ServerDatabase.find(params[:id])
    @server_database.destroy

    respond_to do |format|
      format.html { redirect_to server_databases_url }
    end
  end
end
