class ServerApplicationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /server_applications
  def index
    @server_applications = ServerApplication.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /server_applications/1
  def show
    @server_application = ServerApplication.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /server_applications/new
  def new
    @server_application = ServerApplication.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /server_applications/1/edit
  def edit
    @server_application = ServerApplication.find(params[:id])
  end

  # POST /server_applications
  def create
    @server_application = ServerApplication.new(params[:server_application])

    respond_to do |format|
      if @server_application.save
        format.html { redirect_to @server_application, notice: 'Server application was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /server_applications/1
  def update
    @server_application = ServerApplication.find(params[:id])

    respond_to do |format|
      if @server_application.update_attributes(params[:server_application])
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /server_applications/1
  def destroy
    @server_application = ServerApplication.find(params[:id])
    @server_application.destroy

    respond_to do |format|
      format.html { redirect_to server_applications_url }
    end
  end
end
