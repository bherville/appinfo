class API::V1::ServerApplicationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :exists]

  # GET /server_applications.json
  def index
    if params[:server_id]
      @server_applications = Server.find(params[:server_id]).server_applications
    elsif params[:application_id]
      @server_applications = Application.find(params[:application_id]).server_applications
    else
      @server_applications = ServerApplication.all
    end

    respond_to do |format|
      format.xml   { render xml:  @server_applications, :include => [:application, :server] }
      format.json  { render json: @server_applications, :include => [:application, :server] }
    end
  end

  # GET /server_applications/1.json
  def show
    @server_application = ServerApplication.find(params[:id])
    @server_application.update_last_checked

    respond_to do |format|
      format.json
    end
  end

  def create
    server = Server.find_by_id(params[:server_application][:server])
    server ||= Server.find_by_name(params[:server_application][:server].downcase)

    application = Application.find_by_id(params[:server_application][:application])
    application ||= Application.find_by_name(params[:server_application][:application])

    params[:server_application][:server] = server
    params[:server_application][:application] = application

    @server_application = current_user.server_applications.new(params[:server_application])


    if @server_application.save
      respond_to do |format|
        format.xml   { render xml:  @server_application, status: :created, location: @server_application }
        format.json  { render json: @server_application, status: :created, location: @server_application }
      end
    else
      respond_to do |format|
        format.xml   { render xml:  @server_application.errors, status: :unprocessable_entity }
        format.json  { render json: @server_application.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /server_applications/1/edit
  def edit
    @server_application = ServerApplication.find(params[:id])
  end

  # PUT /server_applications/1.json
  def update
    @server_application = ServerApplication.find(params[:id])

    respond_to do |format|
      if @server_application.update_attributes(params[:server_application])
        format.html { redirect_to @server_application, notice: 'Application version was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @server_application.errors, status: :unprocessable_entity }
      end
    end
  end

  def exists
    server = Server.find_by_id(params[:server])
    server ||= Server.find_by_name(params[:server].downcase)

    application = Application.find_by_id(params[:application])
    application ||= Application.find_by_name(params[:application])

    server_application = ServerApplication.find_by_application_id_and_server_id application.id, server.id
    server_application.update_last_checked if server_application

    respond_to do |format|
      format.json { render json: server_application }
    end
  end
end
