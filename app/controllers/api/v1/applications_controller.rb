class API::V1::ApplicationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /applications
  # GET /applications.json
  def index
    if params[:server_id]
      @applications = Server.find(params[:server_id]).applications
    else
      @applications = Application.all
    end


    respond_to do |format|
      format.xml   { render xml:  @applications }
      format.json
    end
  end

  # GET /applications/1.json
  def show
    if params[:name] && params[:app_type] && params[:version]
      conditions = {
          :name => params[:name],
          :app_type => params[:app_type],
          :version => params[:version]
      }

      @application = Application.first(:conditions => conditions)
    else
      @application = Application.find_by_id(params[:id])
      @application ||= Application.find_by_name(params[:id])
    end

    @application.update_last_checked if @application


    respond_to do |format|
      format.xml   { render xml:  @application }
      format.json
    end
  end

  # GET /applications/new
  # GET /applications/new.json
  def new
    @application = Application.new

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed New Action - #{request.url}"

    respond_to do |format|
      format.xml   { render xml:  @application }
      format.json  { render json: @application }
    end
  end

  # GET /applications/1/edit
  def edit
    @application = Application.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Edit Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Edited #{@application.name}"
  end

  # POST /applications
  # POST /applications.json
  def create
    @application = current_user.applications.new(params[:application])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Create Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Created #{@application.name}"

    respond_to do |format|
      if @application.save
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@application.name} succeeded"

        format.xml { render xml: @application, status: :created, location: @application }
        format.json { render json: @application, status: :created, location: @application }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@application.name} failed"

        format.xml { render xml: @application.errors, status: :unprocessable_entity }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /applications/1
  # PUT /applications/1.json
  def update
    @application = Application.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Update Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Update #{@application.name}"

    respond_to do |format|
      if @application.update_attributes(params[:application])
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{@application.name} succeeded"

        format.xml { head :no_content }
        format.json { head :no_content }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{@application.name} failed"

        format.xml { render xml: @application.errors, status: :unprocessable_entity }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applications/1
  # DELETE /applications/1.json
  def destroy
    @application = Application.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Destroy Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Destroy #{@application.name}"

    @application.destroy

    respond_to do |format|
      format.xml { head :no_content }
      format.json { head :no_content }
    end
  end

  def exists
    application = Application.find_by_id(params[:application])
    application ||= Application.find_by_name(params[:application])

    application.update_last_checked if application

    respond_to do |format|
      format.json { render json: application }
    end
  end
end
