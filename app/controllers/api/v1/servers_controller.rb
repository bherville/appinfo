class API::V1::ServersController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :applications]

  # GET /servers
  def index
    @servers = Server.all

    if params[:show_applications]
      respond_to do |format|
        format.xml   { render xml:  @servers, :include => { :application_versions => { :include => :application } } }
        format.json  { render json: @servers, :include => { :application_versions => { :include => :application } } }
      end
    else
      respond_to do |format|
        format.xml   { render xml:  @servers }
        format.json  { render json: @servers }
      end
    end
  end

  # GET /servers/1
  def show
    if params[:id] == 'show'
      @server = Server.find_by_name(params[:name].downcase)
    else
      @server = Server.find_by_id(params[:id])
      @server ||= Server.find_by_name(params[:id].downcase)
    end

    if @server
      @server.update_last_checked

      respond_to do |format|
        format.xml   { render xml: @server }
        format.json
      end
    else
      respond_to do |format|
        format.xml { render xml: {:server => nil} }
        format.json  { render json: {:server => nil} }
      end
    end
  end

  def applications
    @server = nil

    @server = Server.find(params[:server_id])
    @server ||= Server.find(params[:name].downcase)

    respond_to do |format|
      format.xml   { render xml:  @server.applications }
      format.json  { render json: @server.applications }
    end
  end

  def has_application
    server = Server.find_by_id(params[:server_id])
    server ||= Server.find_by_name(params[:server_id].downcase)

    application = Application.find_by_id(params[:application])
    application ||= Application.find_by_name(params[:application])

    has_app = server.applications.include? application

    if has_app
      application_version = ApplicationVersion.find_by_application_id_and_server_id application.id, server.id
      application_version.update_last_checked if application_version
    end


    respond_to do |format|
      format.xml   { render xml:  has_app }
      format.json  { render json: has_app }
    end
  end

  def associate_application
    server = Server.find_by_id(params[:server_id])
    server ||= Server.find_by_name(params[:server_id].downcase)

    application = Application.find_by_id(params[:application])
    application ||= Application.find_by_name(params[:application])


    server.applications << application
    if application && server.save
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Association of #{application.id} and #{server.name} succeeded"

        format.xml   { render xml:  @server, status: :created, location: @server }
        format.json  { render json: @server, status: :created, location: @server }
      end
    else
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Association of #{application.id} and #{server.name} failed"

        format.xml   { render xml:  @server.errors, status: :unprocessable_entity }
        format.json  { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /servers/new
  def new
    @server = Server.new
    @applications = Application.all

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed New Action - #{request.url}"

    respond_to do |format|
      format.xml   { render xml:  @server }
      format.json  { render json: @server }
    end
  end

  # POST /servers
  def create
    @server = current_user.servers.new(params[:server])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Create Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Created #{@server.name}"

    if @server.save
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@server.name} succeeded"

        format.xml   { render xml:  @server, status: :created, location: @server }
        format.json  { render json: @server, status: :created, location: @server }
      end
    else
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@server.name} failed"

        format.xml   { render xml:  @server.errors, status: :unprocessable_entity }
        format.json  { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /servers/1
  # PUT /servers/1.json
  def update
    @server = Server.find(params[:name].downcase)
    @server ||= Server.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Update Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Update #{server.name}"

    if @server.update_attributes(params[:server])
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{server.name} succeeded"

        format.xml   { head :no_content }
        format.json  { head :no_content }
      end
    else
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{server.name} failed"

        format.xml   { render xml:  @server.errors, status: :unprocessable_entity }
        format.json  { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servers/1
  # DELETE /servers/1.json
  def destroy
    @server = Server.find(params[:name].downcase)
    @server ||= Server.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Destroy Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Destroy #{@server.name}"

    if @server.destroy
      respond_to do |format|
        format.xml   { head :no_content }
        format.json  { head :no_content }
      end
    else
      respond_to do |format|
        format.xml   { render xml:  @server.errors, status: :unprocessable_entity }
        format.json  { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end
end
