class API::V1::ApplicationVersionsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :exists]

  # GET /application_versions.json
  def index
    if params[:server_id]
      @application_versions = Server.find(params[:server_id]).application_versions
    elsif params[:application_id]
      @application_versions = Application.find(params[:application_id]).application_versions
    else
      @application_versions = ApplicationVersion.all
    end

    respond_to do |format|
      format.xml   { render xml:  @application_versions, :include => [:application, :server] }
      format.json  { render json: @application_versions, :include => [:application, :server] }
    end
  end

  # GET /application_versions/1.json
  def show
    @application_version = ApplicationVersion.find(params[:id])
    @application_version.update_last_checked

    respond_to do |format|
      format.json
    end
  end

  def create
    server = Server.find_by_id(params[:application_version][:server])
    server ||= Server.find_by_name(params[:application_version][:server].downcase)

    application = Application.find_by_id(params[:application_version][:application])
    application ||= Application.find_by_name(params[:application_version][:application])

    params[:application_version][:server] = server
    params[:application_version][:application] = application

    @application_version = current_user.application_versions.new(params[:application_version])


    if @application_version.save
      respond_to do |format|
        format.xml   { render xml:  @application_version, status: :created, location: @application_version }
        format.json  { render json: @application_version, status: :created, location: @application_version }
      end
    else
      respond_to do |format|
        format.xml   { render xml:  @application_version.errors, status: :unprocessable_entity }
        format.json  { render json: @application_version.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /application_versions/1/edit
  def edit
    @application_version = ApplicationVersion.find(params[:id])
  end

  # PUT /application_versions/1.json
  def update
    @application_version = ApplicationVersion.find(params[:id])

    respond_to do |format|
      if @application_version.update_attributes(params[:application_version])
        format.html { redirect_to @application_version, notice: 'Application version was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @application_version.errors, status: :unprocessable_entity }
      end
    end
  end

  def exists
    server = Server.find_by_id(params[:server])
    server ||= Server.find_by_name(params[:server].downcase)

    application = Application.find_by_id(params[:application])
    application ||= Application.find_by_name(params[:application])

    application_version = ApplicationVersion.find_by_application_id_and_server_id application.id, server.id
    application_version.update_last_checked if application_version

    respond_to do |format|
      format.json { render json: application_version }
    end
  end
end
