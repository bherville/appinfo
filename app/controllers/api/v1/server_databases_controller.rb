class API::V1::ServerDatabasesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :exists]

  # GET /server_databases.json
  def index
    @server_databases = ServerDatabase.all

    respond_to do |format|
      format.json
    end
  end

  # GET /server_databases/1.json
  def show
    @server_database = ServerDatabase.find(params[:id])
    @server_database.update_last_checked if @server_database

    respond_to do |format|
      format.json
    end
  end

  def exists
    @database = Database.find_by_name_and_dbms(params[:database_name], params[:database_dbms])
    @server   = Server.find_by_name(params[:server_name])

    @server_database = ServerDatabase.find_by_server_id_and_database_id(@server.id, @database.id) if @database && @server
    @server_database.update_last_checked if @server_database

    respond_to do |format|
      format.json { render json: @server_database }
    end
  end

  # POST /server_databases.json
  def create
    server = get_server_by_id_or_name params[:server_database][:server]

    if params[:server_database][:database_id]
      database = get_database_by_id_or_name_and_dbms params[:server_database][:database_id]
    else
      database = get_database_by_id_or_name_and_dbms({:name => params[:server_database][:database_name], :dbms => params[:server_database][:database_dbms]})
    end

    @server_database = ServerDatabase.new(
        :server_id    => server.id,
        :database_id  => database.id
    )

    respond_to do |format|
      if @server_database.save
        format.json { render json: @server_database, status: :created, location: @server_database }
      else
        format.json { render json: @server_database.errors, status: :unprocessable_entity }
      end
    end
  end
end
