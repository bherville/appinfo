class API::V1::ServerRelationshipsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  def index
    @server_relationships = ServerRelationship.all

    respond_to do |format|
      format.xml   { render xml:  @server_relationships, :include => [:server_primary, :server_secondary] }
      format.json
    end
  end

  def show
    @server_relationship = ServerRelationship.find(params[:id])
    @server_relationship.update_last_checked if @server_relationship

    respond_to do |format|
      format.json
    end
  end

  def exists
    server_primary    = Server.find_by_name(params[:server_primary])
    server_secondary  = Server.find_by_name(params[:server_secondary])

    @server_relationship = ServerRelationship.find_by_server_primary_id_and_server_secondary_id_and_port(server_primary.id, server_secondary.id, params[:port]) if server_primary && server_secondary && params[:port]
    @server_relationship.update_last_checked if @server_relationship

    respond_to do |format|
      format.json { render json: @server_relationship }
    end
  end

  def create
    server_primary = get_server_by_id_or_name params[:server_relationship][:server_primary]
    server_secondary = get_server_by_id_or_name params[:server_relationship][:server_secondary]

    @server_relationship = ServerRelationship.new(
                                                 :server_primary   => server_primary,
                                                 :server_secondary => server_secondary,
                                                 :port             => params[:server_relationship][:port] ? params[:server_relationship][:port].to_i : nil,
                                                 :description      => params[:server_relationship][:description]
    )

    respond_to do |format|
      if @server_relationship.save
        format.json { render json: @server_relationship, status: :created, location: @server_relationship }
      else
        format.json { render json: @server_relationship.errors, status: :unprocessable_entity }
      end
    end
  end
end
