class API::V1::DatabasesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show, :exists]

  # GET /databases.json
  def index
    @databases = Database.all

    respond_to do |format|
      format.json { render json: @databases, :include => :application }
    end
  end

  # GET /databases/1.json
  def show
    if params[:id]
      @database = Database.find(params[:id])
    else
      @database = Database.find_by_name_and_dbms(params[:name], params[:dbms])
    end

    @database.update_last_checked if @database

    respond_to do |format|
      format.json { render json: @database }
    end
  end

  def exists
    @database = Database.find_by_name_and_dbms(params[:name], params[:dbms])

    @database.update_last_checked if @database

    respond_to do |format|
      format.json { render json: @database }
    end
  end

  # POST /databases.json
  def create
    if params[:database][:application]
      application = Application.find_by_name params[:database][:application]

      params[:database][:application] = application
    end


    @database = Database.new(params[:database])

    respond_to do |format|
      if @database.save
        format.json { render json: @database, status: :created, location: @database }
      else
        format.json { render json: @database.errors, status: :unprocessable_entity }
      end
    end
  end
end
