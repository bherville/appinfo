class API::APIBaseController < ApplicationController
  rescue_from ::ActiveRecord::RecordNotFound, with: :record_not_found
  rescue_from ::ActiveRecord::RecordInvalid, with: :record_invalid
  rescue_from ::NameError, with: :error_occurred
  rescue_from ::ActionController::RoutingError, with: :error_occurred
  rescue_from ::PG::UniqueViolation, with: :record_duplicate

  protected
  def record_not_found(exception)
    render json: APIError::Exceptions.record_not_found(exception).to_json, status: 404
  end

  def record_invalid(exception)
    render json: APIError::Exceptions.record_invalid(exception).to_json, status: 422
  end

  def record_duplicate(exception = nil, options = {})
    execption = ActiveRecord::RecordNotUnique.new(options[:message], exception)
    render json: APIError::Exceptions.record_duplicate(execption).to_json, status: 500
  end

  def error_occurred(exception)
    render json: {error: exception.message}.to_json, status: 500
  end

  def clean_params_create
    attributes = %w(auth_token format action controller)

    attributes.each do |a|
      self.params.delete(a)
    end
  end
end