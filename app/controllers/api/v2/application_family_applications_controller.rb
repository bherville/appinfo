class API::V2::ApplicationFamilyApplicationsController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :application_families, 'Application Family Management'

  swagger_api :index do
    summary 'Fetches all Application Family Applications items'
    notes 'This lists all the Application Family Applications'
    param :path, :application_family_id, :string, :optional, 'Name of Application Family'
  end

  swagger_api :show do
    summary 'Fetches a single Application Family Application item'
    param :path, :id, :string, :required, 'Application Family Name or Application Name'
    param :path, :application_family_id, :string, :required, 'Name of Application Family'
  end

  swagger_api :create do
    summary 'Creates a new Application Family Application item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :application_family_id, :string, :required, 'Application Family Name'
    param :form, 'application_family_application[application_id]', :string, :required, 'Application Name'
    param :form, 'application_family_application[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'application_family_application[active]', :boolean, :optional, 'Whether this Application Family item is active'
  end

  # GET /application_family_applications.json
  def index
    if params[:application_id]
      @application_family_applications = ApplicationFamilyApplication.find_all_by_application_family_id!(ApplicationFamily.find(params[:application_id]).id)
    else
      @application_family_applications = ApplicationFamilyApplication.all
    end

    respond_to do |format|
      format.json
    end
  end

  # GET /application_family_applications/1.json
  def show
    if params[:application_family_id] && params[:id]
      @application_family_application = ApplicationFamilyApplication.find_by_application_family_id_and_application_id!(ApplicationFamily.find(params[:application_family_id]).id, Application.find(params[:id]).id)
    else
      @application_family_application = ApplicationFamilyApplication.find(params[:id])
    end

    @application_family_application.update_last_checked if @application_family_application

    respond_to do |format|
      format.json
    end
  end

  # POST /application_family_applications.json
  def create
    application_family = ApplicationFamily.find(params[:application_family_id])

    params[:application_family_application][:application_id] = Application.find(params[:application_family_application][:application_id]).id

    @application_family_application = application_family.application_family_applications.new(params[:application_family_application])

    respond_to do |format|
      if @application_family_application.save!
        format.json { render json: @application_family_application, status: :created, location: @application_family_application }
      else
        format.json { render json: @application_family_application.errors, status: :unprocessable_entity }
      end
    end
  end
end
