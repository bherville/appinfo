class API::V2::DatabasesController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :database, 'Application Management'

  swagger_api :index do
    summary 'Fetches all Database items'
    notes 'This lists all the Databases'
    param :path, :application_id, :string, :optional, 'Application Name, fetch all Databases for the indicated Application'
    param :path, :server_id, :string, :optional, 'Server Name, fetch all Databases for the indicated Server'
  end

  swagger_api :show do
    summary 'Fetches a single Database item'
    param :path, :id, :string, :required, 'Application Name or Server Name'
    param :path, :application_id, :string, :required, 'Application Name'
    param :path, :server_id, :string, :required, 'Server Name'
  end

  swagger_api :create do
    summary 'Creates a new Database item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :application_id, :string, :required, 'Application Name'
    param :path, :server_id, :string, :required, 'Server Name'
    param :form, 'database[name]', :string, :required, 'Database Name'
    param :form, 'database[dbms]', :string, :optional, 'Database Management System'
    param :form, 'database[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'database[active]', :boolean, :optional, 'Whether this Database item is active'
  end

  # GET /databases.json
  def index
    if params[:server_id]
      @databases = Server.find(params[:server_id]).databases
    elsif params[:application_id]
      @databases = Application.find(params[:application_id]).databases
    else
      @databases = Database.all
    end

    respond_to do |format|
      format.json
    end
  end

  # GET /databases/1.json
  def show
    if params[:server_id]
      server_database = ServerDatabase.find_by_server_id_and_database_id!(Server.find(params[:server_id]).id, Database.find(params[:id]).id)
      server_database.update_last_checked
      @database = server_database.database
    elsif params[:application_id]
      @database = Database.find_by_application_id_and_name!(Application.find(params[:application_id]).id, params[:id])
    else
      @database = Database.find(params[:id])
    end

    @database.update_last_checked if @database

    respond_to do |format|
      format.json
    end
  end

  # POST /databases.json
  def create
    if params[:application_id]
      application = Application.find(params[:application_id])

      @database = application.databases.new(params[:database])
    elsif params[:server_id]
      server = Server.find(params[:server_id])

      @database = server.databases.new(params[:database])
    else
      @database = Database.new(params[:database])
    end


    respond_to do |format|
      if @database.save!
        format.json { render json: @database, status: :created, location: @database }
      else
        format.json { render json: @database.errors, status: :unprocessable_entity }
      end
    end
  end
end
