class API::V2::ServerApplicationsController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :server_application, 'Server Application Management'

  swagger_api :index do
    summary 'Fetches all Server Application items'
    notes 'This lists all the Server Applications'
    param :path, :application_id, :string, :optional, 'Application Name, fetch all Databases for the indicated Application'
    param :path, :server_id, :string, :optional, 'Server Name, fetch all Databases for the indicated Server'
  end

  swagger_api :show do
    summary 'Fetches a single Server Application item'
    param :path, :id, :string, :required, 'Server Application ID, Application Name or Server Name'
    param :path, :application_id, :string, :required, 'Application Name'
    param :path, :server_id, :string, :required, 'Server Name'
  end

  swagger_api :create do
    summary 'Creates a new Server Application item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :application_id, :string, :required, 'Application Name'
    param :path, :server_id, :string, :required, 'Server Name'
    param :form, 'server_application[description]', :string, :optional, 'Server Application Description'
    param :form, 'server_application[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'server_application[active]', :boolean, :optional, 'Whether this Database item is active'
  end

  # GET /server_applications.json
  def index
    if params[:server_id]
      @server_applications = Server.find(params[:server_id]).server_applications
    elsif params[:application_id]
      @server_applications = Application.find(params[:application_id]).server_applications
    else
      @server_applications = ServerApplication.all
    end

    respond_to do |format|
      format.xml   { render xml:  @server_applications, :include => [:application, :server] }
      format.json
    end
  end

  # GET /server_applications/1.json
  def show
    if params[:server_id] && params[:id]
      @server_application = ServerApplication.find_by_application_id_and_server_id!(Application.find(params[:id]).id, Server.find(params[:server_id]).id)
    elsif params[:application_id] && params[:id]
        @server_application = ServerApplication.find_by_application_id_and_server_id!(Application.find(params[:application_id]).id, Server.find(params[:id]).id)
    else
      @server_application = ServerApplication.find(params[:id])
    end
    @server_application.update_last_checked if @server_application

    respond_to do |format|
      format.json
    end
  end

  def create
    if params[:server_id]
      params[:server_application][:application_id] = Application.find(params[:server_application][:application_id]).id

      @server_application = Server.find(params[:server_id]).server_applications.new(params[:server_application])
    elsif params[:application_id]
      params[:server_application][:server_id] = Server.find(params[:server_application][:server_id]).id

      @server_application = Application.find(params[:application_id]).server_applications.new(params[:server_application])
    end


    if @server_application.save!
      respond_to do |format|
        format.xml   { render xml:  @server_application, status: :created, location: @server_application }
        format.json  { render json: @server_application, status: :created, location: @server_application }
      end
    else
      respond_to do |format|
        format.xml   { render xml:  @server_application.errors, status: :unprocessable_entity }
        format.json  { render json: @server_application.errors, status: :unprocessable_entity }
      end
    end
  end
end
