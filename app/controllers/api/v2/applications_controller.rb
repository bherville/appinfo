class API::V2::ApplicationsController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :application, 'Application Management'

  swagger_api :index do
    summary 'Fetches all Application items'
    notes 'This lists all the Versions'
    param :path, :server_id, :string, :optional, 'Server Name, fetch all Applications for the indicated Server'
  end

  swagger_api :show do
    summary 'Fetches a single Application item'
    param :path, :id, :string, :required, 'Application Name or Server Name'
    param :path, :application_id, :string, :optional, 'Application Name'
    param :path, :database_id, :string, :optional, 'Database Name'
    param :path, :server_id, :string, :optional, 'Server Name'
  end

  swagger_api :create do
    summary 'Creates a new Application item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :database_id, :string, :required, 'Database Name'
    param :path, :server_id, :string, :required, 'Server Name'
    param :form, 'application[name]', :string, :required, 'Application Name'
    param :form, 'application[app_type]', :string, :optional, 'Application Type'
    param :form, 'application[sends_emails]', :boolean, :optional, 'Wether this Application sends emails.'
    param :form, 'application[path]', :string, :optional, 'The path to the installed Application'
    param :form, 'application[version_command]', :string, :optional, 'Command to check the version of the Application'
    param :form, 'application[major_application]', :boolean, :optional, 'Whether this Application is a major one'
    param :form, 'application[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'application[active]', :boolean, :optional, 'Whether this Application Family item is active'
  end

  # GET /applications
  # GET /applications.json
  def index
    if params[:server_id]
      @applications = Server.find(params[:server_id]).applications
    elsif params[:application_family_id]
      @applications = ApplicationFamily.find(params[:application_family_id]).applications
    else
      @applications = Application.all
    end

    respond_to do |format|
      format.xml   { render xml:  @applications }
      format.json
    end
  end

  # GET /applications/1.json
  def show
    if params[:database_id]
      database = Database.find(params[:database_id])
      database.update_last_checked
      @application = database.application
    elsif params[:server_id] && params[:id]
      server_application = ServerApplication.find_by_application_id_and_server_id!(Application.find(params[:id]).id, Server.find(params[:server_id]).id)
      server_application.update_last_checked
      @application = server_application.application
    else
      @application = Application.find(params[:id])
    end

    @application.update_last_checked if @application


    respond_to do |format|
      format.xml   { render xml:  @application }
      format.json
    end
  end

  # GET /applications/new
  # GET /applications/new.json
  def new
    @application = Application.new

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed New Action - #{request.url}"

    respond_to do |format|
      format.xml   { render xml:  @application }
      format.json  { render json: @application }
    end
  end

  # POST /applications
  # POST /applications.json
  def create
    if params[:server_id]
      server = Server.find(params[:server_id])

      @application = server.applications.new(params[:application])
    elsif params[:database_id]
      database = Database.find(params[:database_id])

      @application = database.build_application(params[:application])
    else
      @application = current_user.applications.new(params[:application])
    end

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Create Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Created #{@application.name}"

    respond_to do |format|
      if @application.save!
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@application.name} succeeded"

        format.xml { render xml: @application, status: :created, location: @application }
        format.json { render json: @application, status: :created, location: @application }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@application.name} failed"

        format.xml { render xml: @application.errors, status: :unprocessable_entity }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end
end
