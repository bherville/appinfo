class API::V2::ServerRelationshipsController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :server_relationship, 'Server Relationship Management'

  swagger_api :index do
    summary 'Fetches all Server Relationship items'
    notes 'This lists all the Server Relationships'
    param :path, :server_id, :string, :optional, 'Primary Server Name'
  end

  swagger_api :show do
    summary 'Fetches a single Server Relationship item'
    param :path, :server_id, :string, :required, 'Primary Server Name'
    param :path, :id, :string, :required, 'Server Relationship ID or Secondary Server Name'
    param :query, :port, :integer, :required, 'The destination port'
  end

  swagger_api :create do
    summary 'Creates a new Server Relationship item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :server_id, :string, :required, 'Primary Server Name'
    param :form, 'server_relationship[server_secondary_id]', :string, :required, 'Secondary Server Name'
    param :form, 'server_relationship[port]', :integer, :required, 'The destination port'
    param :form, 'server_relationship[description]', :string, :optional, 'A description of this Server Relationship'
    param :form, 'server_relationship[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'server_relationship[active]', :boolean, :optional, 'Whether this Database item is active'
  end

  def index
    if params[:server_id]
      @server_relationships = ServerRelationship.find_all_by_server_primary_id!(Server.find(params[:server_id]).id)
    else
      @server_relationships = ServerRelationship.all
    end

    respond_to do |format|
      format.xml   { render xml:  @server_relationships, :include => [:server_primary, :server_secondary] }
      format.json
    end
  end

  def show
    if params[:server_id] && params[:id] && params[:port]
      @server_relationship = ServerRelationship.find_by_server_primary_id_and_server_secondary_id_and_port!(Server.find(params[:server_id]).id, Server.find(params[:id]).id, params[:port])
    else
      @server_relationship = ServerRelationship.find(params[:id])
    end


    respond_to do |format|
      if @server_relationship
        @server_relationship.update_last_checked

        format.json
      else
        raise ActiveRecord::RecordNotFound.new("#{ServerRelationship} not found.")
      end
    end
  end

  def create
    if params[:server_id]
      params[:server_relationship][:server_secondary_id] = Server.find(params[:server_relationship][:server_secondary_id]).id

      @server_relationship = Server.find(params[:server_id]).server_relationships.new(params[:server_relationship])
    end

    respond_to do |format|
      if @server_relationship.save!
        format.json { render json: @server_relationship, status: :created, location: @server_relationship }
      else
        format.json { render json: @server_relationship.errors, status: :unprocessable_entity }
      end
    end
  end
end
