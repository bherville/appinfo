class API::V2::ServersController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :server, 'Server Management'

  swagger_api :index do
    summary 'Fetches all Server items'
    notes 'This lists all the Servers'
    param :path, :server_id, :string, :optional, 'Primary Server Name'
    param :path, :application_id, :string, :required, 'Application Name'
  end

  swagger_api :show do
    summary 'Fetches a single Server Relationship item'
    param :path, :id, :string, :required, 'Server Name'
    param :path, :application_id, :string, :required, 'Application Name'
  end

  swagger_api :create do
    summary 'Creates a new Server Relationship item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :form, 'server[name]', :string, :required, 'Server Name'
    param :form, 'server[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'server[active]', :boolean, :optional, 'Whether this Database item is active'
  end

  # GET /servers
  def index
    if params[:application_id]
      @servers = Application.find(params[:application_id]).servers
    elsif params[:server_id]
      @servers = ServerRelationship.find_all_by_server_primary_id(Server.find(params[:server_id]).id).map { |sr| sr.server_secondary }
    elsif params[:database_id]
      @servers = ServerDatabase.find_all_by_database_id!(Database.find(params[:database_id]).id).map { |sr| sr.server }
    else
      @servers = Server.all
    end

    respond_to do |format|
      format.xml   { render xml:  @servers }
      format.json
    end
  end

  # GET /servers/1
  def show
    @server = Server.find(params[:id])

    @server.update_last_checked

    respond_to do |format|
      format.xml   { render xml: @server }
      format.json
    end
  end

  # POST /servers
  def create
    @server = current_user.servers.new(params[:server])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Create Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Created #{@server.name}"

    if @server.save
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@server.name} succeeded"

        format.xml   { render xml:  @server, status: :created, location: @server }
        format.json  { render json: @server, status: :created, location: @server }
      end
    else
      respond_to do |format|
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@server.name} failed"

        format.xml   { render xml:  @server.errors, status: :unprocessable_entity }
        format.json  { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end
end
