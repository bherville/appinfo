class API::V2::ApplicationFamiliesController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :application_families, 'Application Family Management'

  swagger_api :index do
    summary 'Fetches all Application Families items'
    notes 'This lists all the Application Families'
  end

  swagger_api :show do
    summary 'Fetches a single Application Family item'
    param :path, :id, :string, :required, 'Application Family Name'
  end

  swagger_api :create do
    summary 'Creates a new Server Application item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :form, 'application_family[name]', :string, :required, 'Application Family Name'
    param :form, 'application_family[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'application_family[active]', :boolean, :optional, 'Whether this Application Family item is active'
  end

  # GET /application_families.json
  def index
    @application_families = ApplicationFamily.all

    respond_to do |format|
      format.json { render json: @application_families }
    end
  end

  # GET /application_families/1.json
  def show
    @application_family = ApplicationFamily.find(params[:id])

    @application_family.update_last_checked if @application_family

    respond_to do |format|
      format.json { render json: @application_family }
    end
  end

  # POST /application_families.json
  def create
    @application_family = ApplicationFamily.new(params[:application_family])

    respond_to do |format|
      if @application_family.save
        format.json { render json: @application_family, status: :created, location: @application_family }
      else
        format.json { render json: @application_family.errors, status: :unprocessable_entity }
      end
    end
  end
end
