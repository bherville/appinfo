class API::V2::ApplicationVersionsController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :application_version, 'Application Version Management'

  swagger_api :index do
    summary 'Fetches all Application Version items'
    notes 'This lists all the Application Versions'
    param :path, :application_id, :string, :optional, 'Application Name, fetch all Application Versions for the indicated Application'
    param :path, :server_id, :string, :optional, 'Server Name, fetch all Application Versions for the indicated Server'
  end

  swagger_api :show do
    summary 'Fetches a single Application Version item'
    param :path, :id, :string, :required, 'Application Name or Server Name'
    param :path, :application_id, :string, :optional, 'Application Name'
    param :path, :server_id, :string, :optional, 'Server Name'
  end

  swagger_api :create do
    summary 'Creates a new Application Version item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :application_id, :string, :required, 'Application Name'
    param :path, :server_id, :string, :required, 'Server Name'
    param :form, 'application_version[application_version]', :string, :optional, 'The version to set for this Application Version'
    param :form, 'application_version[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'application_version[active]', :boolean, :optional, 'Whether this Application Family item is active'
  end

  swagger_api :update do
    summary 'Updates an Application Version item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :application_id, :string, :required, 'Application Name'
    param :path, :server_id, :string, :required, 'Server Name'
    param :form, 'application_version[application_version]', :string, :required, 'The version to set for this Application Version'
  end

  # GET /application_versions.json
  def index
    if params[:server_id]
      @application_versions = Server.find(params[:server_id]).application_versions
    elsif params[:application_id]
      @application_versions = Application.find(params[:application_id]).application_versions
    else
      @application_versions = ApplicationVersion.all
    end

    respond_to do |format|
      format.xml   { render xml:  @application_versions }
      format.json
    end
  end

  # GET /application_versions/1.json
  def show
    if params[:application_id] && params[:server_id]
      server_application = ServerApplication.find_by_application_id_and_server_id!(Application.find(params[:application_id]).id, Server.find(params[:server_id]).id)
      server_application.update_last_checked
      @application_version = ApplicationVersion.find_by_server_application_id!(server_application.id)
    else
      @application_version = ApplicationVersion.find(params[:id])
    end

    @application_version.update_last_checked if @application_version

    respond_to do |format|
      format.json
    end
  end

  def create
    if params[:application_id] && params[:server_id]
      server_application = ServerApplication.find_by_application_id_and_server_id!(Application.find(params[:application_id]).id, Server.find(params[:server_id]).id)

      @application_version = server_application.build_application_version(params[:application_version])
    end


    if @application_version.save!
      respond_to do |format|
        format.xml   { render xml:  @application_version, status: :created, location: @application_version }
        format.json  { render json: @application_version, status: :created, location: @application_version }
      end
    else
      respond_to do |format|
        format.xml   { render xml:  @application_version.errors, status: :unprocessable_entity }
        format.json  { render json: @application_version.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /application_versions/1.json
  def update
    if params[:application_id] && params[:server_id] && params[:application_version]
      server_application = ServerApplication.find_by_application_id_and_server_id!(Application.find(params[:application_id]).id, Server.find(params[:server_id]).id)

      @application_version = server_application.application_version
    end

    respond_to do |format|
      if @application_version.update_attributes!(params[:application_version])
        format.json { head :no_content }
      else
        format.json { render json: @application_version.errors, status: :unprocessable_entity }
      end
    end
  end
end
