class API::V2::ServerDatabasesController < API::APIBaseController
  before_filter :authenticate_user!, :except => [:index, :show]

  swagger_controller :server_database, 'Server Database Management'

  swagger_api :index do
    summary 'Fetches all Server Database items'
    notes 'This lists all the Server Databases'
    param :path, :database_id, :string, :optional, 'Database Name'
    param :path, :server_id, :string, :optional, 'Server Name'
  end

  swagger_api :show do
    summary 'Fetches a single Server Database item'
    param :path, :id, :string, :required, 'Server Database ID, Database Name or Server Name'
    param :path, :database_id, :string, :optional, 'Database Name'
    param :path, :server_id, :string, :optional, 'Server Name'
  end

  swagger_api :create do
    summary 'Creates a new Server Database item'
    param :form, :auth_token, :string, :required, "An authorized user's token."
    param :path, :server_id, :string, :required, 'Server Name'
    param :form, :database_id, :string, :required, 'Database Name'
    param :form, 'server_database[ignore_active]', :boolean, :optional, 'Whether to ignore the active parameter'
    param :form, 'server_database[active]', :boolean, :optional, 'Whether this Database item is active'
  end

  # GET /server_databases.json
  def index
    if params[:server_id]
      @server_databases = Server.find(params[:server_id]).server_databases
    elsif params[:database_id]
      @server_databases = ServerDatabase.find_all_by_database_id!(Database.find(params[:database_id]).id)
    else
      @server_databases = ServerDatabase.all
    end

    respond_to do |format|
      format.json
    end
  end

  # GET /server_databases/1.json
  def show
    if params[:server_id] && params[:id]
      @server_database = ServerDatabase.find_by_server_id_and_database_id!(Server.find(params[:server_id]).id, Database.find(params[:id]).id)
    else
      @server_database = ServerDatabase.find(params[:id])
    end

    @server_database.update_last_checked if @server_database

    respond_to do |format|
      format.json
    end
  end

  # POST /server_databases.json
  def create
    if params[:server_id]
      params[:server_database][:database_id] = Database.find(params[:server_database][:database_id]).id

      @server_database = Server.find(params[:server_id]).server_databases.new(params[:server_database])
    end

    respond_to do |format|
      if @server_database.save!
        format.json { render json: @server_database, status: :created, location: @server_database }
      else
        format.json { render json: @server_database.errors, status: :unprocessable_entity }
      end
    end
  end
end
