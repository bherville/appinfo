class ApplicationVersionsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /application_versions
  # GET /application_versions.json
  # GET /application_versions.csv
  def index
    @application_versions = ApplicationVersion.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @application_versions }
      format.csv  { render csv: @application_versions, add_methods: [:server, :application], exclude: [:application_id, :server_id] }
    end
  end

  # GET /application_versions/1
  # GET /application_versions/1.json
  def show
    @application_version = ApplicationVersion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @application_version }
    end
  end

  # GET /application_versions/1/edit
  def edit
    @application_version = ApplicationVersion.find(params[:id])
  end

  # POST /application_versions
  # POST /application_versions.json
  def create
    @application_version = ApplicationVersion.new(params[:application_version])

    respond_to do |format|
      if @application_version.save
        format.html { redirect_to @application_version, notice: 'Application version was successfully created.' }
        format.json { render json: @application_version, status: :created, location: @application_version }
      else
        format.html { render action: "new" }
        format.json { render json: @application_version.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /application_versions/1
  # PUT /application_versions/1.json
  def update
    @application_version = ApplicationVersion.find(params[:id])

    respond_to do |format|
      if @application_version.update_attributes(params[:application_version])
        format.html { redirect_to @application_version, notice: 'Application version was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @application_version.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /application_versions/1
  # DELETE /application_versions/1.json
  def destroy
    @application_version = ApplicationVersion.find(params[:id])
    @application_version.destroy

    respond_to do |format|
      format.html { redirect_to application_versions_url }
      format.json { head :no_content }
    end
  end
end
