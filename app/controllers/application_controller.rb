class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_timezone

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = exception.message
    redirect_to root_url
  end

  def set_timezone
    Time.zone = current_user.time_zone if current_user
  end

  private
  def get_server_by_id_or_name(server_id_or_name)
    if server_id_or_name =~ /\A[-+]?\d+\z/
      server = Server.find(server_id_or_name.to_i)
    else
      server = Server.find_by_name(server_id_or_name)
    end

    server
  end

  def get_database_by_id_or_name_and_dbms(database_id_or_name_and_dbms)
    if database_id_or_name_and_dbms =~ /\A[-+]?\d+\z/
      database = Database.find(database_id_or_name_and_dbms.to_i)
    else
      database = Database.find_by_name_and_dbms(database_id_or_name_and_dbms[:name], database_id_or_name_and_dbms[:dbms])
    end

    database
  end
end
