class ServerRelationshipsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /server_relationships
  def index
    @server_relationships = ServerRelationship.all

    respond_to do |format|
      format.html # index.html.erb
      format.csv  { render csv: @server_relationships, add_methods: [:server_primary, :server_secondary], exclude: [:server_primary_id, :server_secondary_id] }
    end
  end

  # GET /server_relationships/1
  def show
    @server_relationship = ServerRelationship.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /server_relationships/new
  def new
    @server_relationship = ServerRelationship.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /server_relationships/1/edit
  def edit
    @server_relationship = ServerRelationship.find(params[:id])
  end

  # POST /server_relationships
  def create
    @server_relationship = ServerRelationship.new(params[:server_relationship])

    respond_to do |format|
      if @server_relationship.save
        format.html { redirect_to @server_relationship, notice: 'Server relationship was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /server_relationships/1
  def update
    @server_relationship = ServerRelationship.find(params[:id])

    respond_to do |format|
      if @server_relationship.update_attributes(params[:server_relationship])
        format.html { redirect_to @server_relationship, notice: 'Server relationship was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /server_relationships/1
  def destroy
    @server_relationship = ServerRelationship.find(params[:id])
    @server_relationship.destroy

    respond_to do |format|
      format.html { redirect_to server_relationships_url }
    end
  end
end
