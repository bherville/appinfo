class TokenAuthenticationsController < ApplicationController
  def create
    @user = User.find(params[:user_id])
    @user.reset_authentication_token!

    respond_to do |format|
      format.html { redirect_to edit_user_registration_path(@user) }
      format.js
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.authentication_token = nil
    @user.save

    respond_to do |format|
      format.html { redirect_to edit_user_registration_path(@user) }
      format.js
    end
  end
end
