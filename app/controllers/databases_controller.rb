class DatabasesController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /databases
  def index
    @databases = Database.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /databases/1
  def show
    @database = Database.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.csv  { render csv: @database, add_methods: [:server] }
    end
  end

  # GET /databases/new
  def new
    @database = Database.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /databases/1/edit
  def edit
    @database = Database.find(params[:id])
  end

  # POST /databases
  def create
    @database = Database.new(params[:database])

    respond_to do |format|
      if @database.save
        format.html { redirect_to @database, notice: 'Database was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /databases/1
  def update
    @database = Database.find(params[:id])

    respond_to do |format|
      if @database.update_attributes(params[:database])
        format.html { redirect_to @database, notice: 'Database was updated created.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /databases/1
  def destroy
    @database = Database.find(params[:id])
    @database.destroy

    respond_to do |format|
      format.html { redirect_to databases_url }
    end
  end
end
