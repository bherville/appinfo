class ServersController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /servers
  # GET /servers.csv
  def index
    @servers = Server.all.sort_by! {|u| u.name}

    respond_to do |format|
      format.html # index.html.erb
      format.csv  { render csv: @servers }
    end
  end

  # GET /servers/1
  # GET /servers/1.csv
  def show
    @server = Server.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /servers/new
  def new
    @server = Server.new
    @applications = Application.all :order => 'name ASC'

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed New Action - #{request.url}"

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /servers/1/edit
  def edit
    @server = Server.find(params[:id])
    @applications = Application.all :order => 'name ASC'

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Edit Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Edited #{@server.name}"
  end

  # POST /servers
  def create
    @server = current_user.servers.new(params[:server])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Create Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Created #{@server.name}"

    respond_to do |format|
      if @server.save
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@server.name} succeeded"

        format.html { redirect_to @server, notice: 'Server was successfully created.' }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@server.name} failed"

        format.html { render action: "new" }
      end
    end
  end

  # PUT /servers/1
  def update
    @server = Server.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Update Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Update #{@server.name}"

    respond_to do |format|
      if @server.update_attributes(params[:server])
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{@server.name} succeeded"

        format.html { redirect_to @server, notice: 'Server was successfully updated.' }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{@server.name} failed"

        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /servers/1
  def destroy
    @server = Server.find(params[:id])
    @server.destroy

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Destroy Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Destroy #{@server.name}"

    respond_to do |format|
      format.html { redirect_to servers_url }
    end
  end
end
