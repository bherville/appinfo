class ApplicationsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index, :show]

  # GET /applications
  # GET /applications.csv
  def index
    @applications = Application.all

    respond_to do |format|
      format.html # index.html.erb
      format.csv  { render csv: @applications }
    end
  end

  # GET /applications/1
  def show
    @application = Application.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /applications/new
  def new
    @application = Application.new
    @servers = Server.all(:order => 'name')

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed New Action - #{request.url}"

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /applications/1/edit
  def edit
    @application = Application.find(params[:id])
    @servers = Server.all

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Edit Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Edited #{@application.name}"
  end

  # POST /applications
  def create
    @application = current_user.applications.new(params[:application])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Create Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Created #{@application.name}"

    respond_to do |format|
      if @application.save
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@application.name} succeeded"

        format.html { redirect_to @application, notice: 'Application was successfully created.' }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Creation of #{@application.name} failed"

        format.html { render action: "new" }
      end
    end
  end

  # PUT /applications/1
  def update
    @application = Application.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Update Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Update #{@application.name}"

    respond_to do |format|
      if @application.update_attributes(params[:application])
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{@application.name} succeeded"

        format.html { redirect_to @application, notice: 'Application was successfully updated.' }
      else
        logger.debug "#{current_user.email} @ #{request.remote_ip} - Update of #{@application.name} failed"

        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /applications/1
  def destroy
    @application = Application.find(params[:id])

    logger.info "#{current_user.email} @ #{request.remote_ip} - Accessed Destroy Action - #{request.url}"
    logger.debug "#{current_user.email} @ #{request.remote_ip} - Destroy #{@application.name}"

    @application.destroy

    respond_to do |format|
      format.html { redirect_to applications_url }
    end
  end
end
